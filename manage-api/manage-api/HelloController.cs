﻿using Google.Apis.Auth.OAuth2;
using System;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Reflection;

namespace manage_api.Controllers
{
    [RoutePrefix("hello")]
    public class HelloController : ApiController
    {
        //  https://localhost:44353/api/values
        [HttpGet]
        [Route("")]
        public async Task<string> Get(string name)
        {
            try
            {
                var targetAudience = "971423989910-gvq8kv7moq7s7e0mk7l7jqtals5iae86.apps.googleusercontent.com";

                var credentialsFile = LoadCredentialsFile();
                var saCredential = ServiceAccountCredential.FromServiceAccountData(credentialsFile);

                var tokenOptions = OidcTokenOptions
                    .FromTargetAudience(targetAudience)
                    .WithTokenFormat(OidcTokenFormat.Standard);

                var oidcToken = await saCredential.GetOidcTokenAsync(tokenOptions);
                var token = await oidcToken.GetAccessTokenAsync();

                using (var httpClient = new HttpClient())
                {
                    var uri = $"https://custom-segment-app-dot-custom-segment-poc.uc.r.appspot.com/hello_http?name={name}";
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await httpClient.GetStringAsync(uri);
                    Console.WriteLine(response);

                    return response;
                }
            }
            catch (AggregateException ex)
            {
                foreach (var err in ex.InnerExceptions)
                {
                    Console.WriteLine("ERROR: " + err.Message);
                }
                return "FAIL";
            }
        }

        private Stream LoadCredentialsFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var credentialsFilePath = "custom-segment-poc-17ed8721e5ec.json";
            var resourceName = $"manage_api.{ credentialsFilePath }";

            try
            {
                return assembly.GetManifestResourceStream(resourceName);
            }
            catch (Exception e)
            {
                throw new Exception($"Impossible to get the file '{resourceName}' : {e.Message}");
            }

        }
    }
}
