from flask import Flask, request
import requests

app = Flask(__name__)

#
# curl https://custom-segment-app-dot-custom-segment-poc.uc.r.appspot.com/hello_http?name=jeremy
# curl -H 'Authorization: Bearer $(gcloud auth print-identity-token)' https://custom-segment-app-dot-custom-segment-poc.uc.r.appspot.com/hello_http?name=jeremy
#
# curl -H 'Authorization: Bearer AIzaSyCb_Cob2G0p_aa_IBMWQi6QVtV2s380bU0' https://us-central1-custom-segment-poc.cloudfunctions.net/hello_http?name=jeremy
#
@app.route('/hello_http')
def hello_http():
    name = request.args.get('name')

    if name is None:
        return 'No name provided in query string'

    cloudFunctionUrl = 'https://us-central1-custom-segment-poc.cloudfunctions.net/hello_http'
    jwt = get_token(cloudFunctionUrl)

    headers = {
        'Authorization': f'Bearer {jwt}'
    }
    queryParams = { 
        'name': name 
    }
    response = requests.get(cloudFunctionUrl, params=queryParams, headers=headers)

    return response.text

def get_token(target_url):
    # https://cloud.google.com/functions/docs/securing/authenticating#service-to-function
    metadata_server_url = 'http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?audience='

    token_full_url = metadata_server_url + target_url
    token_headers = {
      'Metadata-Flavor': 'Google'
    }

    token_response = requests.get(token_full_url, headers=token_headers)
    return token_response.text

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
