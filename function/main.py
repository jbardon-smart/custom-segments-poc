from flask import escape

def hello_http(request):
    # Client ID: 971423989910-vjo89kosp017h773brkqn06nr8pvblc9.apps.googleusercontent.com
    # Client secret: 623dkAWNs3EtVYEkex07bAYV
    #
    # gcloud auth application-default login
    #
    # curl https://us-central1-custom-segment-poc.cloudfunctions.net/hello_http?name=jeremy
    # curl -H 'Authorization: Bearer $(gcloud auth print-identity-token)' https://us-central1-custom-segment-poc.cloudfunctions.net/hello_http?name=jeremy
    #
    # curl -H 'Authorization: Bearer AIzaSyCb_Cob2G0p_aa_IBMWQi6QVtV2s380bU0' https://us-central1-custom-segment-poc.cloudfunctions.net/hello_http?name=jeremy
    #
    request_args = request.args

    if request_args and 'name' in request_args:
        name = request_args['name']
    else:
        name = 'World'

    return 'Hello {}!'.format(escape(name))